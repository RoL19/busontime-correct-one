//
//  ViewController.swift
//  ViewControllers_test
//
//  Created by Otto Linden on 22/04/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
//        var lightDarkColor = UIColor(red: 21/255, green: 38/255, blue: 66/255, alpha: 1)
//        tabBarr.barTintColor = lightDarkColor
//        navigationController?.navigationBar.barTintColor = lightDarkColor
        
        setupTabBar()
    }
    
    func setupTabBar() {
        let starImage = UIImage(named: "Star")
        let startSelected = UIImage(named: "SatarSelected")
        
        let searchImage = UIImage(named: "Search")
        
        let settingsImage = UIImage(named: "SettingsWheel")
        
        let MapScreenController = createNavController(vc: MapScreen(), item: UITabBarItem(title: nil, image: searchImage, selectedImage: nil))
        let favoriteController = createNavController(vc: FavoriteViewController(), item: UITabBarItem(title: nil, image: starImage, selectedImage: startSelected))
        let settingController = createNavController(vc: SettingsScreen(), item: UITabBarItem(title: nil, image: settingsImage, selectedImage: nil))

        viewControllers = [MapScreenController, favoriteController, settingController]
        
        guard let items = tabBar.items else { return }
        
        for item in items {
            item.imageInsets = UIEdgeInsets(top: 7, left: 0, bottom: -7, right: 0)
        }
        
        
    }

}

extension UITabBarController {
    func createNavController(vc: UIViewController, item: UITabBarItem) -> UINavigationController{
        let viewController = vc
        let navController = UINavigationController(rootViewController: viewController)
        
        navController.tabBarItem = item
        
        return navController
    }
}

