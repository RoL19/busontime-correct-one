//
//  InfoScreen.swift
//  ViewControllers_test
//
//  Created by Otto Linden on 22/04/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit

class InfoScreen: UIViewController {
    var showRouteButton: UIButton!
    var TotalBusses: UILabel!
    var activeBusses: UILabel!
    var activeBussesOnRoute: Int!
    var activeBussesLabel: UILabel!
    var background: UIImageView!
    var notifactions: UILabel!
    var notiBool = "On"
    
    var startImage: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackgound()
        
        self.title = FavoriteViewController.selectedRoute! as String
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        view.backgroundColor = .white
        
        tabBarController?.tabBar.isHidden = false
        
        createRouteButton()
        
        setupActiveBussesLabel()
        
        notifactionsOnThisRoute()
        
        if FavoriteViewController.favoriteRoutes.contains(FavoriteViewController.selectedRoute) {
            startImage = UIImage(named: "StarSelected")?.withRenderingMode(.alwaysOriginal)
//            navigationItem.rightBarButtonItem?.tintColor = .blue
        } else {
            startImage = UIImage(named: "Star")?.withRenderingMode(.alwaysOriginal)
//            navigationItem.rightBarButtonItem?.tintColor = .gray
        }

        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: startImage, style: .plain, target: self, action: #selector(favorited))
        
        self.tabBarController?.tabBar.layer.borderWidth = 0.25
        self.tabBarController?.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBarController?.tabBar.clipsToBounds = true
    }
    
    @objc func favorited() {
        if FavoriteViewController.favoriteRoutes.contains(FavoriteViewController.selectedRoute) {
            guard var index = FavoriteViewController.favoriteRoutes.firstIndex(of: FavoriteViewController.selectedRoute) else { return }
            FavoriteViewController.favoriteRoutes.remove(at: index)
            print(FavoriteViewController.favoriteRoutes)
        } else {
            FavoriteViewController.favoriteRoutes.append(FavoriteViewController.selectedRoute)
            print(FavoriteViewController.favoriteRoutes)
        }
        
        if FavoriteViewController.favoriteRoutes.contains(FavoriteViewController.selectedRoute) {
            navigationItem.rightBarButtonItem?.image = UIImage(named: "StarSelected")?.withRenderingMode(.alwaysOriginal)
        } else {
            navigationItem.rightBarButtonItem?.image = UIImage(named: "Star")?.withRenderingMode(.alwaysOriginal)
        }
    }
    
    func setBackgound() {
        background = UIImageView()
        view.addSubview(background)
        guard let backgroundImage = UIImage(named: "Föli") else { return }
        background.backgroundColor = UIColor(patternImage: backgroundImage)
        print("infoScreen.setBackground calling")
        
        background.translatesAutoresizingMaskIntoConstraints = false
        background.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        background.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        background.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        background.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        background.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        background.contentMode = .scaleAspectFit
//        background.backgroundColor = .red
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func setupActiveBussesLabel() {
        activeBussesLabel = UILabel()
        view.addSubview(activeBussesLabel)
        
        activeBussesLabel.translatesAutoresizingMaskIntoConstraints = false
        activeBussesLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25).isActive = true
        activeBussesLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -220).isActive = true
        activeBussesLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 75).isActive = true
        
        activeBussesLabel.text = "Active Busses: \(Int.random(in: 0...4))"
        activeBussesLabel.font = UIFont.systemFont(ofSize: 22)
        activeBussesLabel.textAlignment = .center
        activeBussesLabel.clipsToBounds = true
        
        activeBussesLabel.layer.cornerRadius = 14.0
//        activeBussesLabel.layer.borderWidth = 1.0
        
        activeBussesLabel.textColor = .white
//        activeBussesLabel.backgroundColor = .blue
    }

    
    func createRouteButton() {
        showRouteButton = UIButton()
        view.addSubview(showRouteButton)
        
        showRouteButton.translatesAutoresizingMaskIntoConstraints = false
        showRouteButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        showRouteButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -40).isActive = true
        showRouteButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 85).isActive = true
        showRouteButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -85).isActive = true
        
        showRouteButton.clipsToBounds = true
        showRouteButton.layer.cornerRadius = 17
        showRouteButton.backgroundColor = .blue
        
        showRouteButton.setTitle("Show Route On Map", for: .normal)
        showRouteButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        showRouteButton.addTarget(self, action: #selector(showOnMapTapped), for: .touchUpInside)
        
    }
    
    @objc func showOnMapTapped() {
        var vc = MapScreen()
        
        MapScreen.selectedRoute = title
        MapScreen.busLabel.text = title
//        MapScreen.showOnMap = true
        tabBarController?.selectedIndex = 0
//        MapScreen.checkIfShowOnMap()
        print("tapped")
    }
    
    func notifactionsOnThisRoute() {
        notifactions = UILabel()
        view.addSubview(notifactions)
        
        activeBussesLabel = UILabel()
        view.addSubview(activeBussesLabel)
        
        activeBussesLabel.translatesAutoresizingMaskIntoConstraints = false
        activeBussesLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 220).isActive = true
        activeBussesLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25).isActive = true
        activeBussesLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 75).isActive = true
        
        var random = Int.random(in: 0...1)
        if random == 0 { notiBool = "On" }
        if random == 1 { notiBool = "Off" }
        activeBussesLabel.text = "Notifications: \(notiBool)"
        activeBussesLabel.font = UIFont.systemFont(ofSize: 22)
        activeBussesLabel.textAlignment = .center
        activeBussesLabel.clipsToBounds = true
        
        activeBussesLabel.layer.cornerRadius = 14.0
//        activeBussesLabel.layer.borderWidth = 1.0
        
        activeBussesLabel.textColor = .white
    }
}
