//
//  SettingsScreen.swift
//  BusOnTIme_Official
//
//  Created by Otto Linden on 20/05/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit

class SettingsScreen: UIViewController {
    var notiSwitch: UISwitch!
    var notiLabel: UILabel!
    
    var locationSwitch: UISwitch!
    var locationLabel: UILabel!
    
    var feedbackSwitch: UISwitch!
    var feedbackLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        self.navigationItem.title = "Settings"
        
        fixNotiSwitch()
        
        // Do any additional setup after loading the view.
    }
    
    func fixNotiSwitch() {
        notiSwitch = UISwitch()
        view.addSubview(notiSwitch)
        
        notiSwitch.translatesAutoresizingMaskIntoConstraints = false
        notiSwitch.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 25).isActive = true
        notiSwitch.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        
        notiLabel = UILabel()
        view.addSubview(notiLabel)
        
        notiLabel.translatesAutoresizingMaskIntoConstraints = false
        notiLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        notiLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        
        notiLabel.text = "Notifications: "
        notiLabel.font = UIFont.boldSystemFont(ofSize: 20)
        
        
        locationSwitch = UISwitch()
        view.addSubview(locationSwitch)
        
        locationSwitch.translatesAutoresizingMaskIntoConstraints = false
        locationSwitch.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 125).isActive = true
        locationSwitch.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        
        locationSwitch.isOn = true
        
        locationLabel = UILabel()
        view.addSubview(locationLabel)
        
        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        locationLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 130).isActive = true
        locationLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        
        locationLabel.text = "Location: "
        locationLabel.font = UIFont.boldSystemFont(ofSize: 20)
        
        
        feedbackSwitch = UISwitch()
        view.addSubview(feedbackSwitch)
        
        feedbackSwitch.isOn = true
        
        feedbackSwitch.translatesAutoresizingMaskIntoConstraints = false
        feedbackSwitch.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 225).isActive = true
        feedbackSwitch.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -30).isActive = true
        
        feedbackLabel = UILabel()
        view.addSubview(feedbackLabel)
        
        feedbackLabel.translatesAutoresizingMaskIntoConstraints = false
        feedbackLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 230).isActive = true
        feedbackLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        
        feedbackLabel.text = "Send feedback: "
        feedbackLabel.font = UIFont.boldSystemFont(ofSize: 20)

    }
}
