//
//  ViewController.swift
//  BusOnTime
//
//  Created by Otto Linden on 12/03/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit
import MapKit

// API Stuff
    typealias Route = [RouteStuff]

    struct RouteStuff: Codable {
        let routeID, agencyID, routeShortName, routeLongName: String
        let routeType: Int
        let routeColor, routeTextColor: String
        
        enum CodingKeys: String, CodingKey {
            case routeID = "route_id"
            case agencyID = "agency_id"
            case routeShortName = "route_short_name"
            case routeLongName = "route_long_name"
            case routeType = "route_type"
            case routeColor = "route_color"
            case routeTextColor = "route_text_color"
        }
    }

    struct FoliVm: Codable {
        let sys, status: String
        let servertime: Int
        let result: Result
    }

    struct Result: Codable {
        let responsetimestamp: Int
        let producerref, responsemessageidentifier: String
        let status, moredata: Bool
        let vehicles: [String: Vehicle]
    }

    struct Vehicle: Codable {
        let recordedattime, validuntiltime: Int
        let monitored, incongestion, inpanic: Bool
        let vehicleref: String
        let operatorref: String?
        let linkdistance: Int?
        let lineref, directionref, publishedlinename, originref: String?
        let originname, destinationref, destinationname: String?
        let originaimeddeparturetime, destinationaimedarrivaltime: Int?
        let longitude, latitude: Double?
        let delay: String?
        let delaysecs: Int?
        let blockref, nextStoppointref: String?
        let nextVisitnumber: Int?
        let nextStoppointname: String?
        let vehicleatstop: Bool?
        let nextDestinationdisplay: String?
        let nextAimedarrivaltime, nextExpectedarrivaltime, nextAimeddeparturetime, nextExpecteddeparturetime: Int?
        let onwardcalls: [Onwardcall]?
        let tripref: String?
        
        enum CodingKeys: String, CodingKey {
            case recordedattime, validuntiltime, monitored, incongestion, inpanic, vehicleref, operatorref, linkdistance, lineref, directionref, publishedlinename, originref, originname, destinationref, destinationname, originaimeddeparturetime, destinationaimedarrivaltime, longitude, latitude, delay, delaysecs, blockref
            case nextStoppointref = "next_stoppointref"
            case nextVisitnumber = "next_visitnumber"
            case nextStoppointname = "next_stoppointname"
            case vehicleatstop
            case nextDestinationdisplay = "next_destinationdisplay"
            case nextAimedarrivaltime = "next_aimedarrivaltime"
            case nextExpectedarrivaltime = "next_expectedarrivaltime"
            case nextAimeddeparturetime = "next_aimeddeparturetime"
            case nextExpecteddeparturetime = "next_expecteddeparturetime"
            case onwardcalls
            case tripref = "__tripref"
        }
    }

    struct Onwardcall: Codable {
        let stoppointref: String
        let visitnumber: Int
        let stoppointname: String
        let aimedarrivaltime, expectedarrivaltime, aimeddeparturetime, expecteddeparturetime: Int
    }


    typealias trips = [WelcomeElement]

    struct WelcomeElement: Codable {
        let serviceID, tripID, tripHeadsign, routeID: String
        let directionID: Int
        let blockID, shapeID: String
        let wheelchairAccessible: Int
        
        enum CodingKeys: String, CodingKey {
            case serviceID = "service_id"
            case tripID = "trip_id"
            case tripHeadsign = "trip_headsign"
            case directionID = "direction_id"
            case blockID = "block_id"
            case shapeID = "shape_id"
            case wheelchairAccessible = "wheelchair_accessible"
            case routeID = "route_id"
        }
    }


    typealias latlong = [Welcome]

    struct Welcome: Codable {
        let lat, lon, traveled: Double
    }

class MapScreen: UIViewController{
    
    // Map stuff
        let mapView = MKMapView()
        var zoomed = false
    
        let locationManager = CLLocationManager()
        let regionInMeters: Double = 12500
        var askt = 0
    
        static var showOnMap = false

    
    // Bus Stuff
        static let busLabel = UITextField()
        static var busNames = [String]()
        static var selectedRoute: String?
        static let busPicker = UIPickerView()
        static var selectedRouteId: String?
        static var RouteShortName = [String]()
        static var selectedTripId = [String]()
        static var selectedShapeId = [String]()
        static var selectedBusLat = [Double]()
        static var selectedBusLong = [Double]()
        static var selectedBusLineLat = [Double]()
        static var selectedBusLineLong = [Double]()
        static var polyLine: MKPolyline!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true

        updateNavigationController()
        setupMapView()
        setupBusLabel()
        setUpBusAPI()
        createBusPicker()
        createToolBar()
        
        print("hmmm")
        self.tabBarController?.tabBar.layer.borderWidth = 0.50
        self.tabBarController?.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBarController?.tabBar.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkRouteID()
        print("hmmmkdfkdf")
    }
    
    @objc func setUpBusAPI() {
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/routes"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                var routes = try JSONDecoder().decode(Route.self, from: data)
                
                MapScreen.busNames.removeAll()
                
                for data in routes {
                    MapScreen.busNames.append(data.routeShortName + " " + data.routeLongName)
                    
                    
                    func checkRoute() {
                        guard let route = MapScreen.selectedRoute else { return }
                        
                        
                        
                        if route == data.routeShortName + " " + data.routeLongName {
                            MapScreen.selectedRouteId = data.routeID
                            
                        } else {
                            //                            print(data.routeShortName + " " + data.routeLongName)
                        }
                    }
                    
                    checkRoute()
                    
                }
                
                
                
                MapScreen.busNames = MapScreen.busNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                
                if MapScreen.selectedRouteId != nil {
                    self.setupTrips()
                }
                
                
            } catch let jsonErr {
                print("Error serializing json 1:", jsonErr)
            }
            }.resume()
    }
    
    func checkRouteID() {
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/routes"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                var routes = try JSONDecoder().decode(Route.self, from: data)
                
                for data in routes {
                    
                    func checkRoute() {
                        guard let route = MapScreen.selectedRoute else { return }
                        
                        
                        
                        if route == data.routeShortName + " " + data.routeLongName {
                            MapScreen.selectedRouteId = data.routeID
                            
                        } else {
//                            print(data.routeShortName + " " + data.routeLongName)
                        }
                    }
                    
                    checkRoute()
                    
                }
                
                MapScreen.busNames = MapScreen.busNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                
                if MapScreen.selectedRouteId != nil {
                    self.setupTrips()
                }
                
                
            } catch let jsonErr {
                print("Error serializing json 2:", jsonErr)
            }
            }.resume()
    }
    
    func setupTrips() {
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/trips/all"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                let Trips = try JSONDecoder().decode(trips.self, from: data)
                
                MapScreen.selectedShapeId.removeAll()
                MapScreen.selectedTripId.removeAll()
                for data in Trips {
                    if MapScreen.selectedRouteId == data.routeID {
                        MapScreen.selectedShapeId.append(data.shapeID)
                        
                        MapScreen.selectedTripId.append(data.tripID)
                    }
                }
                
                
                
                self.setupVm()
                
            } catch let jsonErr {
                print("Error serializing json 3:", jsonErr)
            }
            }.resume()
    }
    
    func setupVm() {
        let jsonUrlString = "https://data.foli.fi/siri/vm"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                var vm = try JSONDecoder().decode(FoliVm.self, from: data)
                
                MapScreen.selectedBusLong.removeAll()
                MapScreen.selectedBusLat.removeAll()
                
                for (key, value) in vm.result.vehicles {
                    
                    func checkTripId() {
                        guard let tripId = value.tripref else { return }
                        if MapScreen.selectedTripId.contains(tripId){
                            MapScreen.selectedBusLat.append(value.latitude!)
                            MapScreen.selectedBusLong.append(value.longitude!)
                        }
                    }
                    
                    checkTripId()
                }
                //            print("done")
                self.setupShape()
            } catch let jsonErr {
                print("Error serializing json 4:", jsonErr)
            }
            }.resume()
    }
    
    func getMostCommonShape(array: [String]) -> [String] {
        //        print(selectedShapeId)
        
        var topShapes = [String]()
        var shapeDictionary: [String: Int] = [:]
        
        for shape in array {
            if let count = shapeDictionary[shape] {
                shapeDictionary[shape] = count + 1
            } else {
                shapeDictionary[shape] = 1
            }
        }
        
        let highestValue = shapeDictionary.values.max()
        
        for (shape, count) in shapeDictionary {
            if shapeDictionary[shape] == highestValue {
                topShapes.append(shape)
                //                print("Shape \(shape)")
            }
        }
        //        print("Top shapes: \(topShapes)")
        return topShapes
    }
    
    
    
    func setupShape() {
        
        var shape = getMostCommonShape(array: MapScreen.selectedShapeId)
        
        if shape.count > 0 {
            let shape = shape[0]
            let jsonUrlString = "https://data.foli.fi/gtfs/v0/shapes/" + shape
                    print(jsonUrlString)
            guard let url = URL(string: jsonUrlString) else { print("url error"); return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                //check err
                //check response status 299 ok
                
                guard let data = data else { print("data error"); return }
                
                do {
                    
                    var shape = try JSONDecoder().decode(latlong.self, from: data)
                    //                print("about to start")
                    
                    func makeLine() {
                        MapScreen.selectedBusLineLat.removeAll()
                        MapScreen.selectedBusLineLong.removeAll()
                        for data in shape {
                            MapScreen.selectedBusLineLat.append(data.lat)
                            MapScreen.selectedBusLineLong.append(data.lon)
                        }
                    }
                    
                    //                if self.selectedShapeId != self.lastSelectedShapeID.first {
                    makeLine()
                    //                } else {
                    //                    print("error sorry line 326")
                    //                }
                    
                    self.ShowLine()
                    self.showAnnotations()
                    
                    
                } catch let jsonErr {
                    print("Error serializing json 5:", jsonErr)
                }
                
                }.resume()
        } else {
            func errorAlert() {
                let ac = UIAlertController(title: "Error", message: "Sorry, this route is no longer used", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Continue", style: .default))
                present(ac, animated: true)
            }
            
            DispatchQueue.main.async { self.mapView.removeOverlays(self.mapView.overlays)
                errorAlert();
                self.mapView.removeAnnotations(self.mapView.annotations)
            }
            
        }
    }
    
    
    
    
    func ShowLine() {
        
        
        var location = [CLLocationCoordinate2D]()
        
        location.removeAll()
        DispatchQueue.main.async {
            self.mapView.removeOverlays(self.mapView.overlays)
        }
        for (long, lat) in zip(MapScreen.selectedBusLineLong, MapScreen.selectedBusLineLat) {
            
            
            location.append(CLLocationCoordinate2D(latitude: lat, longitude: long))
            
        }
        
        MapScreen.polyLine = MKPolyline(coordinates: location, count: location.count)
        
    }
    
    func showAnnotations() {
        
        DispatchQueue.main.async {
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        for (long, lat) in zip(MapScreen.selectedBusLong, MapScreen.selectedBusLat) {
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            DispatchQueue.main.async { self.mapView.addAnnotation(annotation) }
            
            
        }
        
        var regionRect = MapScreen.polyLine.boundingMapRect
        
        
        let wPadding = regionRect.size.width * 0.25
        let hPadding = regionRect.size.height * 0.25
        
        //Add padding to the region
        regionRect.size.width += wPadding
        regionRect.size.height += hPadding
        
        //Center the region on the line
        regionRect.origin.x -= wPadding / 2
        regionRect.origin.y -= hPadding / 2
        
        DispatchQueue.main.async { self.mapView.setRegion(MKCoordinateRegion(regionRect), animated: true) }
        DispatchQueue.main.async { self.mapView.addOverlay(MapScreen.polyLine) }
        
        
    }
    
    func updateNavigationController() {
//        let lightDarkColor = UIColor(red: 21/255, green: 38/255, blue: 66/255, alpha: 1)
//        navigationController?.navigationBar.barTintColor = lightDarkColor
//        
//        let darkColor = UIColor(red: 8/255, green: 27/255, blue: 51/255, alpha: 1)
        view.backgroundColor = .white
    }
    
    func setupMapView() {
        view.addSubview(mapView)
        self.mapView.delegate = self
        mapView.translatesAutoresizingMaskIntoConstraints = false
        
        let startLocation = CLLocationCoordinate2D(latitude: 60.45148, longitude: 22.26869)
        let cordinateRegion = MKCoordinateRegion(center: startLocation, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        
        mapView.setRegion(cordinateRegion, animated: true)
        
    
        mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 80).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        
        mapView.mapType                 = MKMapType.standard
        mapView.isZoomEnabled           = true
        mapView.isScrollEnabled         = true
        
        mapView.layer.cornerRadius      = 15.0
        mapView.clipsToBounds           = true
        mapView.layer.borderWidth       = 1.0
        
        
    }
    
    func setupBusLabel() {
        view.addSubview(MapScreen.busLabel)
        MapScreen.busLabel.translatesAutoresizingMaskIntoConstraints = false
        
        MapScreen.busLabel.leadingAnchor.constraint(equalTo: mapView.leadingAnchor, constant: 15).isActive = true
        MapScreen.busLabel.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -15).isActive = true
        MapScreen.busLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        MapScreen.busLabel.bottomAnchor.constraint(equalTo: mapView.topAnchor, constant: -15).isActive = true
        
        MapScreen.busLabel.adjustsFontSizeToFitWidth = true
        MapScreen.busLabel.placeholder = "Tap here to choose a route! 🚌"
        MapScreen.busLabel.textAlignment = .center
        MapScreen.busLabel.layer.cornerRadius = 15.0
        MapScreen.busLabel.layer.borderWidth = 1.0
    }
    
    func createBusPicker() {
        MapScreen.busPicker.delegate = self
        
        MapScreen.busLabel.inputView = MapScreen.busPicker
    }
    
    func createToolBar() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(MapScreen.dismissKeyboard))
        let resetButton = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(MapScreen.resetPicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([resetButton, spacer, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        MapScreen.busLabel.inputAccessoryView = toolBar
        
        // Customizations
        
        
    }
    
    @objc func resetPicker() {
        MapScreen.busLabel.text = ""
        MapScreen.busPicker.selectRow(0, inComponent: 0, animated: true)
        
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        setUpBusAPI()
    }

}


extension MapScreen: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return MapScreen.busNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return MapScreen.busNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        MapScreen.selectedRoute = MapScreen.busNames[row]
        MapScreen.busLabel.text = MapScreen.selectedRoute
        
    }
    
}

extension MapScreen: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        
        if annotation is MKUserLocation{
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
            if let dequeuedView =       mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as?     MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                pinView = dequeuedView;
            }else{
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                
            }
            return pinView;
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if(overlay is MKPolyline) {
            let polylineRender = MKPolylineRenderer(overlay: overlay)
            polylineRender.strokeColor = UIColor.red
            polylineRender.lineWidth = 5
            
            return polylineRender
        }
        return MKPolylineRenderer()
    }
}
