//
//  FavortieScreen.swift
//  BusOnTime
//
//  Created by Otto Linden on 21/04/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit
class FavoriteViewController: UITableViewController {
    static var favoriteRoutes = [String]()
    static var selectedRoute: String!
    
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.layer.borderWidth = 0.25
        self.tabBarController?.tabBar.layer.borderColor = UIColor.black.cgColor
        self.tabBarController?.tabBar.clipsToBounds = true
        
        print(FavoriteViewController.favoriteRoutes.count)
        
        tabBarController?.tabBar.isHidden = false
        self.navigationItem.title = "Routes"
        self.navigationController?.navigationBar.prefersLargeTitles = true
//        self.navigationItem.title = "Routes"
        setupTableView()
        
        FavoriteViewController.favoriteRoutes = ["1 Lentoasema-Satama", "2 Kohmo-Länsinummi", "702 Salo-Turku", "6 Lieto-Naantali"]
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        tableView.reloadData()
    }
    
    func setupTableView(){
        //Registers a class for use in creating new table cells.
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
    }
}



extension FavoriteViewController {
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            if indexPath.section == 0 {
                if FavoriteViewController.favoriteRoutes.count == 1 {
                    FavoriteViewController.favoriteRoutes.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    self.tableView.reloadData()
                } else {
                    FavoriteViewController.favoriteRoutes.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    self.tableView.reloadData()
                }
            } else {
                let ac = UIAlertController(title: "Error", message: "Sorry it's only possible to delete routes from \"Favorite Routes\"", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Okay", style: .default))
                present(ac, animated: true)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.backgroundColor = UIColor.init(red: 220/250.0, green: 220/250.0, blue: 220/250.0, alpha: 1.0)
        if section == 0 {
            label.text = "Favorite Routes"
        } else {
            label.text = "All Routes"
        }
        label.textColor = .black
        label.font =  UIFont.boldSystemFont(ofSize: 16.0)
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if FavoriteViewController.favoriteRoutes.count == 0 && section == 0{
            return 0
        } else {
            return 25
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return FavoriteViewController.favoriteRoutes.count
        }
        return MapScreen.busNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = "\(FavoriteViewController.favoriteRoutes[indexPath.row])"
        } else {
            cell.textLabel?.text = "\(MapScreen.busNames[indexPath.row])"
        }
        cell.textLabel?.textAlignment = .left
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if FavoriteViewController.favoriteRoutes.count == 0 && indexPath.section == 0{
            return 150
        } else {
            return 40
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var vc = InfoScreen()
        
        if indexPath.section == 1 {
        FavoriteViewController.selectedRoute = MapScreen.busNames[indexPath.row]
        navigationController?.pushViewController(vc, animated: false)
        } else if indexPath.section == 0 {
            if FavoriteViewController.favoriteRoutes.count == 0 {
                
            } else {
                FavoriteViewController.selectedRoute = FavoriteViewController.favoriteRoutes[indexPath.row]
                navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
}
